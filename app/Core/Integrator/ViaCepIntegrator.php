<?php

namespace App\Core\Integrator;

class ViaCepIntegrator
{
    private string $endpoint;

    /**
     * @param string $cep
     * @return string Json
     */
    public function dispatch(string $cep): string
    {
        $this->setup($cep);
        return response()
            ->json($this->makeRequest());
    }

    /**
     * @param string $cep
     * @return string
     */
    private function setup(string $cep): void
    {
        $this->endpoint = "viacep.com.br/ws/{$cep}/json/";
    }

    /**
     * @return string
     */
    private function makeRequest(): string
    {
        $curlHandler = curl_init();
        curl_setopt_array($curlHandler, [
            CURLOPT_URL => $this->endpoint,
            CURLOPT_RETURNTRANSFER => true,
        ]);
        $response = curl_exec($curlHandler);
        curl_close($curlHandler);
        return json_encode($response, true);
    }
}
