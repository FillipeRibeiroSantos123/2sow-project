<?php

namespace App\Services\Citizens;

use Illuminate\Support\Collection;
use App\Models\Citizens\CitizensRepository;

final class GetCitizens
{
    protected CitizensRepository $repository;

    public function __construct(CitizensRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Retrieve all citizens ordenaded by name.
     *
     * @return Collection|null
     */
    public function index()
    {
        return $this->repository->getAllSorted('asc');
    }
}
