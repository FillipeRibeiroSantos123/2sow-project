<?php

namespace App\Services\Citizens\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CreateCitizenValidator
{
    public function validate(Request $request)
    {
        Validator::make($request->toArray(), [
            'cpf' => 'bail|required|string|size:11,unique:citizens',
            'nome' => 'bail|string|nullable',
            'sobrenome' => 'bail|string|nullable',
            'email' => 'bail|nullable|email',
            'celular' => 'bail|string|nullable|max:11',
            'cep' => 'bail|required|string|size:8',
            'logradouro' => 'bail|string|nullable',
            'bairro' => 'bail|string|nullable',
            'cidade' => 'bail|string|nullable',
            'uf' => 'bail|string|nullable|max:2',
        ])->validate();
    }
}
