<?php

namespace App\Services\Citizens\Validator;

use Illuminate\Support\Facades\Validator;

class GetCitizenWithDocumentValidator
{
    public function validate(string $cpf)
    {
        Validator::make(['cpf' => $cpf], [
            'cpf' => 'bail|required|string|size:11',
        ])->validate();
    }
}
