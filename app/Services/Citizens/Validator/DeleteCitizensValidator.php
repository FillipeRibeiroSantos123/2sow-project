<?php

namespace App\Services\Citizens\Validator;

use Illuminate\Support\Facades\Validator;

class DeleteCitizensValidator
{
    public function validate(int $id)
    {
        Validator::make(['id' => $id], [
            'id' => 'integer|required',
        ])->validate();
    }
}
