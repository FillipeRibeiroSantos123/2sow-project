<?php

namespace App\Services\Citizens;

use App\Models\Citizens\Citizens;
use App\Models\Citizens\CitizensRepository;

final class GetCitizenWithDocument
{
    protected CitizensRepository $repository;

    public function __construct(CitizensRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Retrieve a citizen searched by CPF.
     *
     * @return Citizens|null
     */
    public function index(string $document)
    {
        $citizen = $this->repository->getByKey('cpf', $document);
        if (is_null($citizen)) {
            throw new Exception("Citizen with CPF {$document} not found.");
        }
        return $citizen;
    }
}
