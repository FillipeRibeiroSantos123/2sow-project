<?php

namespace App\Services\Citizens;

use App\Models\Citizens\Citizens;
use App\Models\Citizens\CitizensRepository;
use Illuminate\Http\Request;

final class UpdateCitizen
{
    protected CitizensRepository $repository;

    public function __construct(CitizensRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Retrieve a citizen searched by CPF.
     * @param Request $request
     * @return Citizens|null
     */
    public function index(array $data)
    {
        $citizen = $this->repository->getById($data['id']);
        if (is_null($citizen)) {
            throw new \Exception("Citizen not found.");
        }

        $citizen->nome = $data['nome'] ?? $citizen->nome;
        $citizen->sobrenome = $data['sobrenome'] ?? $citizen->sobrenome;
        $citizen->email = $data['email'] ?? $citizen->email;
        $citizen->celular = $data['celular'] ?? $citizen->celular;
        $citizen->logradouro = $data['logradouro'] ?? $citizen->logradouro;
        $citizen->bairro = $data['bairro'] ?? $citizen->bairro;
        $citizen->cidade = $data['cidade'] ?? $citizen->cidade;
        $citizen->uf = $data['uf'] ?? $citizen->uf;
        $this->repository->save($citizen);

        return $citizen;
    }
}
