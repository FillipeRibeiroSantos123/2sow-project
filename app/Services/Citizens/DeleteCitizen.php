<?php

namespace App\Services\Citizens;

use App\Models\Citizens\Citizens;
use App\Models\Citizens\CitizensRepository;
use Illuminate\Http\Request;

final class DeleteCitizen
{
    protected CitizensRepository $repository;

    public function __construct(CitizensRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Delete one Citizen
     * @param Integer $id
     * @return Citizens|null
     */
    public function index(int $id)
    {
        $citizen = $this->repository->getById($id);
        if (is_null($citizen)) {
            throw new Exception("Citizen not found.");
        }
        $this->repository->delete($citizen);
        return "Citizen deleted successfully";
    }
}
