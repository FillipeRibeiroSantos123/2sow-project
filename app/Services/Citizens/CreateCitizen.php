<?php

namespace App\Services\Citizens;

use App\Models\Citizens\Citizens;
use App\Models\Citizens\CitizensRepository;

final class CreateCitizen
{
    protected CitizensRepository $repository;

    public function __construct(CitizensRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Retrieve a citizen searched by CPF.
     * @param Request $request
     * @return Citizens|null
     */
    public function index(array $data)
    {
        $citizen = new Citizens();
        $citizen->cpf = $data['cpf'];
        $citizen->nome = $data['nome'] ?? null;
        $citizen->sobrenome = $data['sobrenome'] ?? null;
        $citizen->email = $data['email'] ?? null;
        $citizen->celular = $data['celular'] ?? null;
        $citizen->cep = $data['cep'];
        $citizen->logradouro = $data['logradouro'] ?? null;
        $citizen->bairro = $data['bairro'] ?? null;
        $citizen->cidade = $data['cidade'] ?? null;
        $citizen->uf = $data['uf'] ?? null;
        $this->repository->save($citizen) ?? null;

        return $citizen;
    }
}
