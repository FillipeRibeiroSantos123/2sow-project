<?php

namespace App\Http\Controllers\Citizens;

use App\Models\Citizens\Citizens;
use App\Http\Controllers\Controller;
use App\Services\Citizens\CreateCitizen;
use App\Services\Citizens\Validator\CreateCitizenValidator;
use Illuminate\Http\Request;

final class CreateCitizensController extends Controller
{
    protected CreateCitizen $createCitizenService;

    public function __construct(CreateCitizen $createCitizenService)
    {
        $this->createCitizenService = $createCitizenService;
    }

    /**
     * Create citizen
     * @param Request $request
     * @return Citizens
     */
    public function index(Request $request)
    {
        (new CreateCitizenValidator())->validate($request);
        $response = $this->createCitizenService->index($request->toArray());
        return response()->json([
            'data' => $response
        ]);
    }
}
