<?php

namespace App\Http\Controllers\Citizens;

use App\Models\Citizens\Citizens;
use App\Http\Controllers\Controller;
use App\Services\Citizens\GetCitizenWithDocument;
use App\Services\Citizens\Validator\GetCitizenWithDocumentValidator;
use Illuminate\Http\Request;

final class GetCitizenWithDocumentController extends Controller
{
    protected GetCitizenWithDocument $getCitizenService;

    public function __construct(GetCitizenWithDocument $getCitizenService)
    {
        $this->getCitizenService = $getCitizenService;
    }

    /**
     * Get specific citizen from database
     * @param Request $request
     * @param string $cpf
     * @return Citizens
     */
    public function index(Request $request, string $cpf)
    {
        (new GetCitizenWithDocumentValidator())->validate($cpf);
        $response = $this->getCitizenService->index($cpf);
        return response()->json([
            'data' => $response
        ]);
    }
}
