<?php

namespace App\Http\Controllers\Citizens;

use App\Models\Citizens\Citizens;
use App\Http\Controllers\Controller;
use App\Services\Citizens\DeleteCitizen;
use App\Services\Citizens\Validator\DeleteCitizensValidator;
use Illuminate\Http\Request;

final class DeleteCitizensController extends Controller
{
    protected DeleteCitizen $deleteCitizenService;

    public function __construct(DeleteCitizen $deleteCitizenService)
    {
        $this->deleteCitizenService = $deleteCitizenService;
    }

    /**
     * Create citizen
     * @param Request $request
     * @param int $id
     * @return Citizens
     */
    public function index(Request $request, int $id)
    {
        (new DeleteCitizensValidator())->validate($id);
        $response = $this->deleteCitizenService->index($id);
        return response()->json([
            'data' => $response
        ]);
    }
}
