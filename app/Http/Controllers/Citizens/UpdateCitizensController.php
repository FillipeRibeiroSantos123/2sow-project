<?php

namespace App\Http\Controllers\Citizens;

use App\Models\Citizens\Citizens;
use App\Http\Controllers\Controller;
use App\Services\Citizens\UpdateCitizen;
use App\Services\Citizens\Validator\UpdateCitizenValidator;
use Illuminate\Http\Request;

final class UpdateCitizensController extends Controller
{
    protected UpdateCitizen $updateCitizenService;

    public function __construct(UpdateCitizen $updateCitizenService)
    {
        $this->updateCitizenService = $updateCitizenService;
    }

    /**
     * Create citizen
     * @param Request $request
     * @return Citizens
     */
    public function index(Request $request)
    {
        (new UpdateCitizenValidator())->validate($request);
        $response = $this->updateCitizenService->index($request->toArray());
        return response()->json([
            'data' => $response
        ]);
    }
}
