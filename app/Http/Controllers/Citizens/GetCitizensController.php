<?php

namespace App\Http\Controllers\Citizens;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Services\Citizens\GetCitizens;

final class GetCitizensController extends Controller
{
    protected GetCitizens $getCitizensService;

    public function __construct(GetCitizens $getCitizensService)
    {
        $this->getCitizensService = $getCitizensService;
    }

    /**
     * Get all citizens from database
     *
     * @return Collection
     */
    public function index()
    {
        $response = $this->getCitizensService->index();
        return response()->json([
            'data' => $response
        ]);
    }
}
