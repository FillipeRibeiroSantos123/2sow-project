<?php

namespace App\Http\Controllers\Integration;

use App\Http\Controllers\Controller;
use App\Core\Integrator\ViaCepIntegrator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

final class ViaCepIntegratorController extends Controller
{
    /**
     * Make a request from Via Cep Api
     *
     * @return Collection
     */
    public function index(string $cep)
    {
        $this->validateCep($cep);
        $response = (new ViaCepIntegrator())->dispatch($cep);
        return response()->json([
            'data' => $response
        ]);
    }

    public function validateCep(string $cep)
    {
        Validator::make(['cep' => $cep], [
            'cep' => 'required|string|max:8'
        ])->validate();
    }
}
