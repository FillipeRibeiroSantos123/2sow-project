<?php

namespace App\Models\Citizens;

use App\Core\Repository;
use Illuminate\Support\Collection;

class CitizensRepository extends Repository
{
    public function getModel(): string
    {
        return Citizens::class;
    }

    /**
     * Retrieve all citizens ordenaded.
     *
     * @param string $sort
     * @return Collection|null
     */
    public function getAllSorted(string $sort = 'asc')
    {
        return $this->newQuery()
            ->orderBy('name', $sort)
            ->get();
    }
}
