<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Citizens\GetCitizensController;
use App\Http\Controllers\Citizens\GetCitizenWithDocumentController;
use App\Http\Controllers\Citizens\CreateCitizensController;
use App\Http\Controllers\Citizens\UpdateCitizensController;
use App\Http\Controllers\Citizens\DeleteCitizensController;

use App\Http\Controllers\Integration\ViaCepIntegratorController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/citizens', [GetCitizensController::class, 'index']);
Route::get('/{cpf}/citizen', [GetCitizenWithDocumentController::class, 'index']);
Route::post('/citizen', [CreateCitizensController::class, 'index']);
Route::put('/citizen', [UpdateCitizensController::class, 'index']);
Route::delete('/{id}/citizen', [DeleteCitizensController::class, 'index']);

Route::get('/get-address/{cep}', [ViaCepIntegratorController::class, 'index']);
